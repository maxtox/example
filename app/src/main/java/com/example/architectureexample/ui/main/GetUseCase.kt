package com.example.architectureexample.ui.main

import com.example.architectureexample.domain.BaseUseCase
import com.example.architectureexample.domain.DataEntity

interface GetUseCase: BaseUseCase<DataEntity> {
}