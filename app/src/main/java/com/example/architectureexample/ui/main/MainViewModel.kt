package com.example.architectureexample.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel(
    private val useCase: GetUseCase,
    private val uiMapper: UiMapper
) : ViewModel() {

    val liveData = MutableLiveData<DataUiModel>()

    init {
        val dataEntity = useCase.execute()
        val dataUiModel = uiMapper.map(dataEntity)
        liveData.postValue(dataUiModel)
    }

}