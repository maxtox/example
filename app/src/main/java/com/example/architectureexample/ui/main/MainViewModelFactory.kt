package com.example.architectureexample.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MainViewModelFactory(
    private val useCase: GetUseCase,
    private val uiMapper: UiMapper
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(useCase, uiMapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}