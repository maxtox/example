package com.example.architectureexample.ui.main

import com.example.architectureexample.domain.DataEntity

interface UiMapper {
    fun map(dataEntity: DataEntity): DataUiModel
}

class UiMapperImpl : UiMapper {
    override fun map(dataEntity: DataEntity): DataUiModel {
        return DataUiModel(dataEntity.tempInCelsuis.toString())
    }

}