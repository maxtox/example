package com.example.architectureexample.domain

import com.example.architectureexample.data.DataDTO

interface Repository {
    fun getCelsius(): DataDTO
}