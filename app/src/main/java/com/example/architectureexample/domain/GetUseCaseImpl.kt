package com.example.architectureexample.domain

import com.example.architectureexample.ui.main.GetUseCase

class GetUseCaseImpl(
    private val repository: Repository,
    private val mapper: Mapper
) : GetUseCase {

    override fun execute(): DataEntity {
        val dataDTO = repository.getCelsius()
        val data = mapper.map(dataDTO)
        return data
    }
}