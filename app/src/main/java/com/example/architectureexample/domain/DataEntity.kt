package com.example.architectureexample.domain

data class DataEntity(
    val tempInCelsuis: Double
)