package com.example.architectureexample.domain

interface BaseUseCase<out T : Any> {
    fun execute(): T
}