package com.example.architectureexample.domain

import com.example.architectureexample.data.DataDTO

interface Mapper {
    fun map(dataDTO: DataDTO): DataEntity
}