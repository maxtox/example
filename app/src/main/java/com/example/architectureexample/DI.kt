package com.example.architectureexample

import com.example.architectureexample.data.MapperImpl
import com.example.architectureexample.data.RepositoryImpl
import com.example.architectureexample.domain.GetUseCaseImpl
import com.example.architectureexample.domain.Mapper
import com.example.architectureexample.domain.Repository
import com.example.architectureexample.ui.main.GetUseCase
import com.example.architectureexample.ui.main.UiMapper
import com.example.architectureexample.ui.main.UiMapperImpl

object DI {

    private val mapper: Mapper = MapperImpl()
    private val repository: Repository = RepositoryImpl()

    val useCase: GetUseCase = GetUseCaseImpl(repository, mapper)
    val uiMapper: UiMapper = UiMapperImpl()

}