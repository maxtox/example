package com.example.architectureexample.data

import com.example.architectureexample.domain.Repository

class RepositoryImpl : Repository {

    override fun getCelsius(): DataDTO {
        //мы типа сходили в сеть
        return DataDTO(celsius = "22.5")
    }
}