package com.example.architectureexample.data

data class DataDTO(
    val celsius: String
)