package com.example.architectureexample.data

import com.example.architectureexample.domain.DataEntity
import com.example.architectureexample.domain.Mapper

class MapperImpl : Mapper {

    override fun map(dataDTO: DataDTO): DataEntity {
        return DataEntity(dataDTO.celsius.toDouble())
    }

}